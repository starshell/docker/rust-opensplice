# Rust OpenSplice 

Stable: [![pipeline status](https://gitlab.com/starshell/docker/rust-opensplice/badges/stable/pipeline.svg)](https://gitlab.com/starshell/docker/rust-opensplice/commits/stable) Beta: [![pipeline status](https://gitlab.com/starshell/docker/rust-opensplice/badges/beta/pipeline.svg)](https://gitlab.com/starshell/docker/rust-opensplice/commits/beta) Nightly: [![pipeline status](https://gitlab.com/starshell/docker/rust-opensplice/badges/nightly/pipeline.svg)](https://gitlab.com/starshell/docker/rust-opensplice/commits/nightly)

<img alt="Docker" src="http://starshell.gitlab.io/icons/logos/docker.svg" width="100" height="100" /> <img alt="Rust" src="http://starshell.gitlab.io/icons/logos/rust.svg" width="100" height="100" /> <img alt="Codecov" src="http://iron-oxide.gitlab.io/icons/logos/codecov.svg" width="100" height="100" /> <img alt="OpenSplice" src="http://starshell.gitlab.io/icons/logos/vortex-opensplice.png" width="100" height="100" />

[Official Rust](https://hub.docker.com/_/rust/) with [OpenSplice DDS](http://www.prismtech.com/dds-community). All builds are automated so they are always the most recent images. The nightly image rebuilds every day, both beta and stable rebuild once a week.

## Usage

The images are hosted on GitLab registries.

### GitLab

All of the images can be found in the project [registry](https://gitlab.com/starshell/docker/rust-opensplice/container_registry) and can be used just like images from dockerhub.

    docker pull registry.gitlab.com/starshell/docker/rust-opensplice:stable
    docker pull registry.gitlab.com/starshell/docker/rust-opensplice:beta
    docker pull registry.gitlab.com/starshell/docker/rust-opensplice:nightly

Also available with [codecov](https://codecov.io/) or [kcov](https://github.com/SimonKagstrom/kcov):

    docker pull registry.gitlab.com/starshell/docker/rust-opensplice:stable-kcov
    docker pull registry.gitlab.com/starshell/docker/rust-opensplice:stable-codecov

#### Kcov 

[Cargo kcov](https://github.com/kennytm/cargo-kcov) provides an easy CLI tool just run:

```bash
cargo kcov
```

#### Codecov

To use with CI just replace `image: rust` with the URL of the registry image.

```yaml
.cargo_test_template: &cargo_test
  stage: test
  script:
    - cargo test --verbose --jobs 1 --all

test:stable:cargo:
  image: registry.gitlab.com/starshell/docker/rust-opensplice:stable-codecov
  <<: *cargo_test
  after_script:
    - |
      for file in target/debug/osdds-*[^\.d]; do mkdir -p "target/cov/$(basename $file)"; kcov --exclude-pattern=/.cargo,/usr/lib --verify "target/cov/$(basename $file)" "$file"; done &&
      bash <(curl -s https://codecov.io/bash) -t $CODECOV_TOKEN &&
      echo "Uploaded code coverage"

test:beta:cargo:
  image: registry.gitlab.com/starshell/docker/rust-opensplice:beta
  <<: *cargo_test

test:nightly:cargo:
  image: registry.gitlab.com/starshell/docker/rust-opensplice:nightly
  <<: *cargo_test
```
